import os
import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt
import pickle
from constants import *
#from plotting_style import *
import quantities as pq
from plotting_style import *
from utils import *

settings_name = 'test_rf'


plot_format = 'png'

subtract_tracker_data_vals = [False]

plt.rc('legend',fontsize=6) # using a size in points

# --- set up plots folder ------------------------------------------------------


plots_folder = os.path.join(DATA_PATH, 'plots', 'decode_across', '{}'.format(settings_name))
if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)

output_file_name = 'decode_across_setting_{}.pkl'.format(settings_name)
output_folder = os.path.join(DATA_PATH, 'results', 'decode', settings_name)
output_full_path = os.path.join(output_folder, output_file_name)
res = pickle.load(open(output_full_path, 'rb'))
bin_centers = res['pars']['spike_bin_centers']
df = res['decoding_scores']
experiments = res['pars']['experiments']


for experiment in experiments:

    dx = df[df['experiment'] == experiment]
    sids = dx['session_id'].unique()
    levels = np.linspace(0.5, 1.5, len(sids))
    palette = {s : lighten_color(sns.xkcd_rgb['peach'], levels[i])
               for i, s in enumerate(sids)}

    f, ax = plt.subplots(1, 1, figsize=[4, 4])
    sns.lineplot(data=dx, x='time', y='score', hue='session_id',
                 palette=palette, ci=None, style='laser')
    ax.set_xlabel('Time [s]')
    ax.set_ylabel('Decoding accuracy\nTarget: {}'.format(experiment))

    ax.axhline(0.5, ls=':', color='grey')
    ax.axvline(0, ls='--', color='grey')
    #plot_events(tf, ax)

    ax.set_xlim([bin_centers[0], bin_centers[-1]])
    # ax.set_title(features)
    sns.despine()
    plt.tight_layout()

    plot_name = 'decode_across_single_sessions_{}_{}.{}'.format(settings_name, experiment,
                                                            plot_format)

    f.savefig(os.path.join(plots_folder, plot_name), dpi=400)

    plt.close()



    f, ax = plt.subplots(1, 1, figsize=[4, 4])
    sns.lineplot(data=dx, x='time', y='score', hue='laser',
                 ci=95, style='laser',
                 palette={t : PPC_color for t in ['off', 'on']},
                 dashes=dashes_dict)
    ax.set_xlabel('Time [s]')
    ax.set_ylabel('Decoding accuracy\nTarget: {}'.format(experiment))

    ax.axhline(0.5, ls=':', color='grey')
    ax.axvline(0, ls='--', color='grey')
    #plot_events(tf, ax)
    ax.set_ylim([0.45, 0.8])

    ax.set_xlim([bin_centers[0], bin_centers[-1]])
    #ax.set_xlim([t_start, t_stop])
    # ax.set_title(features)
    sns.despine()
    plt.tight_layout()

    plot_name = 'decode_across_average_sessions_{}_{}.{}'.format(settings_name, experiment,
                                                            plot_format)

    f.savefig(os.path.join(plots_folder, plot_name), dpi=400)

    plt.close()
