import os
from load_data import get_data
import pandas as pd
import numpy as np
import pickle
from sklearn.model_selection import StratifiedKFold
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import SGDClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import balanced_accuracy_score
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import LabelEncoder
from constants import *

settings_name = 'test_rf'

min_units = 10
min_trials_per_class = 8
decoder_name = 'SGD'
n_estimators = 500
n_splits = 5
n_repeats = 5

data = get_data()
binned_spikes = data['binned_spikes']
tf = data['tf']
time_bin_centers = data['time_bin_centers']
session_ids = data['session_ids']
valid_sessions = data['valid_sessions']

experiments = ['hit_vs_error',
               'hit_vs_miss',
               'error_vs_miss']


df = pd.DataFrame(columns=['session_id', 'laser', 'experiment',
                           'decoder', 'time', 'score', 'n_neurons'])

for experiment in experiments:
    if experiment == 'hit_vs_error':
        responses = ['hit', 'error']
    elif experiment == 'hit_vs_miss':
        responses = ['hit', 'miss']
    elif experiment == 'error_vs_miss':
        responses = ['error', 'miss']

    decdat = {s : {} for s in valid_sessions}
    for sess_id in valid_sessions:

        # vc = tf[(tf['session_id'] == sess_id) &
        #          (np.isin(tf['response'], responses))].groupby('laser')['response'].value_counts()

        # select trials
        for laser in ['on', 'off']:
            tf_sel = tf[tf['session_id'] == sess_id]
            mask = np.logical_and(tf_sel['laser'] == laser,
                                  np.isin(tf_sel['response'], responses))

            ts = tf_sel[(tf_sel['laser'] == laser) &
                    (np.isin(tf_sel['response'], responses))]

            # make target
            target = tf_sel[mask]['response']
            vc = target.value_counts()

            ll = LabelEncoder()
            y = ll.fit_transform(target)

            X = binned_spikes[sess_id][mask, :, :]
            decdat[sess_id][laser] = {}
            decdat[sess_id][laser]['X'] = X
            decdat[sess_id][laser]['y'] = y
            decdat[sess_id][laser]['vc'] = vc

    selected_sessions = []
    for sess_id in valid_sessions:
        cond1 = decdat[sess_id]['off']['vc'].min() > min_trials_per_class
        cond2 = decdat[sess_id]['on']['vc'].min() > min_trials_per_class
        if cond1 and cond2:
            selected_sessions.append(sess_id)


    for sess_id in selected_sessions:
        for i, time in enumerate(time_bin_centers):

            X_on = decdat[sess_id]['on']['X'][:, :, i]
            y_on = decdat[sess_id]['on']['y']
            X_off = decdat[sess_id]['off']['X'][:, :, i]
            y_off = decdat[sess_id]['off']['y']

            # print('- Repeat {}/{}'.format(repeat, n_repeats))

            kfold_on = StratifiedKFold(n_splits=n_splits, shuffle=True,
                                    random_state=1)

            split_off = kfold_on.split(X_off, y_off)
            split_on = kfold_on.split(X_on, y_on)

            kfold_scores = {'off' : [], 'on' : []}
            y_test_all, y_pred_all = [], []

            for fold, [(tr_off, ts_off), (tr_on, ts_on)] in enumerate(zip(split_off, split_on)):

                if decoder_name == 'random_forest' :
                    decoder = RandomForestClassifier(
                        n_estimators=n_estimators)
                elif decoder_name == 'SGD' :
                    decoder = SGDClassifier()
                else :
                    raise NotImplementedError

                X_train_off = X_off[tr_off, :]
                X_test_off = X_off[ts_off, :]
                y_train_off = y_off[tr_off]
                y_test_off = y_off[ts_off]

                X_test_on = X_on[ts_on, :]
                y_test_on = y_on[ts_on]

                ss = StandardScaler()
                X_train_off = ss.fit_transform(X_train_off)
                X_test_off = ss.transform(X_test_off)

                decoder.fit(X_train_off, y_train_off)
                y_pred_off = decoder.predict(X_test_off)
                y_pred_on = decoder.predict(X_test_on)

                # TODO balanced?
                scoring_function = balanced_accuracy_score
                score_off = scoring_function(y_test_off, y_pred_off)
                score_on = scoring_function(y_test_on, y_pred_on)

                kfold_scores['off'].append(score_off)
                kfold_scores['on'].append(score_on)

            for laser in ['off', 'on']:
                mean_score = np.mean(kfold_scores[laser])

                row = [sess_id, laser, experiment,
                       decoder_name, time, mean_score,
                       X.shape[1]]

                df.loc[df.shape[0], :] = row



numeric_cols = ['score', 'time']

for col in numeric_cols :
    df[col] = pd.to_numeric(df[col])


pars = {'settings_name' : settings_name,
        'data_version' : DATA_VERSION,
        'experiments' : experiments,
        'min_trials_per_class' : min_trials_per_class,
        'min_units' : min_units,
        'decoder_name' : decoder_name,
        'n_splits' : n_splits,
        'spike_bin_centers' : time_bin_centers,
        'n_repeats' : n_repeats}


out = {'pars' : pars,
       'decoding_scores' : df}

output_file_name = 'decode_across_setting_{}.pkl'.format(settings_name)
output_folder = os.path.join(DATA_PATH, 'results', 'decode', settings_name)
output_full_path = os.path.join(output_folder, output_file_name)

if not os.path.isdir(output_folder) :
    os.makedirs(output_folder)

print('Saving output to {}'.format(output_full_path))
pickle.dump(out, open(output_full_path, 'wb'))

