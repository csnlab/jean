import os
from constants import *
from loadmat import *
import pandas as pd
from load_data import *

# load data
data = loadmat(os.path.join(DATA_PATH, '{}.mat'.format(DATA_VERSION)))

# make binned_spikes dictionary
mats = data['scores']
labels = data['labels']
valid_sessions = data['valid_sessions']
len(mats)
len(labels)

for i in range(20):
    assert mats[i].shape[0] == labels[i].shape[0]


data = get_data()
binned_spikes = data['binned_spikes']
tf = data['tf']
time_bin_centers = data['time_bin_centers']
session_ids = data['session_ids']
valid_sessions = data['valid_sessions']


for sess_id in valid_sessions:

    X = binned_spikes[sess_id][:, :, 0]

    tf_sel = tf[tf['session_id'] == sess_id]

    assert X.shape[0] == tf_sel.shape[0]


