import os
from constants import *
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import pickle
import numpy as np
from plotting_style import *

settings_name = 'mar1'

save_plots = True
plot_format = 'png'
dpi = 400
plot_folder = os.path.join(DATA_PATH, 'plots')
if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder, exist_ok=True)

output_file_name = 'jeanfeatimp_{}.pkl'.format(settings_name)
output_folder = os.path.join(DATA_PATH, 'results', 'pseudodec', settings_name)
output_full_path = os.path.join(output_folder, output_file_name)
results = pickle.load(open(output_full_path, 'rb'))

df = results['feature_importances']



df = df.groupby(['unit_id', 'experiment', 'time']).mean().reset_index().sort_values(by=['unit_id', 'experiment'])


df['time'] = df['time']



presence_experiments = ['multisensory_presence',
               'unisensory_presence',
               'visual_presence',
               'tactile_presence']

hitmiss_experiments = ['multisensory_hitmiss',
               'visual_hitmiss',
               'tactile_hitmiss',
               'unisensory_hitmiss']


time = 0.25

for experiments in [presence_experiments, hitmiss_experiments]:


    fi1 = df[(df['time'] == time) & (df['experiment'] == experiments[0])]
    fi2 = df[(df['time'] == time) & (df['experiment'] == experiments[1])]
    fi3 = df[(df['time'] == time) & (df['experiment'] == experiments[2])]
    fi4 = df[(df['time'] == time) & (df['experiment'] == experiments[3])]


    seluid = list(set.intersection(set(fi1['unit_id'].__array__()),
                     set(fi2['unit_id'].__array__()),
                     set(fi3['unit_id'].__array__()),
                     set(fi4['unit_id'].__array__())))

    fi1 = fi1[np.isin(fi1['unit_id'], seluid)]
    fi2 = fi2[np.isin(fi2['unit_id'], seluid)]
    fi3 = fi3[np.isin(fi3['unit_id'], seluid)]
    fi4 = fi4[np.isin(fi4['unit_id'], seluid)]


    np.testing.assert_array_equal(fi1['unit_id'], fi2['unit_id'])
    np.testing.assert_array_equal(fi2['unit_id'], fi3['unit_id'])
    np.testing.assert_array_equal(fi3['unit_id'], fi4['unit_id'])

    nf = pd.DataFrame()
    nf['unit_id'] = fi1['unit_id']
    nf['imp1'] = fi1['svm_perm_imp'].__array__()
    nf['imp2'] = fi2['svm_perm_imp'].__array__()
    nf['imp3'] = fi3['svm_perm_imp'].__array__()
    nf['imp4'] = fi4['svm_perm_imp'].__array__()


    nf = nf.sort_values(by='imp1', ascending=False)

    f, ax = plt.subplots(4, 1, sharex=True, sharey=True, figsize=[6, 8])

    ax[0].bar(x=np.arange(nf.shape[0]), height=nf['imp1'], color=experiment_palette[experiments[0]])
    ax[1].bar(x=np.arange(nf.shape[0]), height=nf['imp2'], color=experiment_palette[experiments[1]])
    ax[2].bar(x=np.arange(nf.shape[0]), height=nf['imp3'], color=experiment_palette[experiments[2]])
    ax[3].bar(x=np.arange(nf.shape[0]), height=nf['imp4'], color=experiment_palette[experiments[3]])

    ax[0].set_ylabel('Permutation importance\nat t={} for\n{}'.format(time, experiment_label[experiments[0]]))
    ax[1].set_ylabel('Permutation importance\nat t={} for\n{}'.format(time, experiment_label[experiments[1]]))
    ax[2].set_ylabel('Permutation importance\nat t={} for\n{}'.format(time, experiment_label[experiments[2]]))
    ax[3].set_ylabel('Permutation importance\nat t={} for\n{}'.format(time, experiment_label[experiments[3]]))

    ax[3].set_xlabel('Neuron')
    sns.despine()
    plt.tight_layout()

    if save_plots :
        plot_name = 'svm_perm_feat_imp_settings_{}_{}.{}'.format(settings_name, experiments[0].split('_')[1], plot_format)
        f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



