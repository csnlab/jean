import os
from constants import *
from loadmat import *
import pandas as pd
from sklearn.model_selection import StratifiedKFold
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
from utils import *
import pickle
from sklearn.inspection import permutation_importance



"""
- separate high and low saliency and left and right
- get svm coefficient to find important neurons across conditions
- then look at best unisensory decoder vs  
"""


# ----
settings_name = 'mar1'
binsize_in_ms = 100
slide_by_in_ms = 25
min_trials_per_class = 60
min_n_units = 1
n_bootstraps = 200
n_perm_repeats = 100
subpopulation_sizes = [50]
n_splits = 3
decoder_name = 'SVM'
n_estimators = 200
t0 = 0.2
t1 = 0.4


data, trial_data = load_data()
session_ids = trial_data['session_id'].unique()
session_inds = np.arange(data['data'].shape[0])

experiments = ['multisensory_presence',
               'unisensory_presence',
               'visual_presence',
               'tactile_presence',
               'multisensory_hitmiss',
               'visual_hitmiss',
               'tactile_hitmiss',
               'unisensory_hitmiss']

min_trials = {'presence' : 60,
              'hitmiss'  : 25}

output_file_name = 'jeanfeatimp_{}.pkl'.format(settings_name)
output_folder = os.path.join(DATA_PATH, 'results', 'pseudodec', settings_name)
output_full_path = os.path.join(output_folder, output_file_name)

if not os.path.isdir(output_folder) :
    os.makedirs(output_folder)


dfs = []


for experiment in experiments:

    if np.isin(experiment, presence_experiments) :
        min_trials_per_class = min_trials['presence']
    elif np.isin(experiment, hitmiss_experiments) :
        min_trials_per_class = min_trials['hitmiss']

    sel_sess, sel_sess_ind, sel_sess_id, sel_trials = select_sessions(data,
                                                            trial_data,
                                                            experiment,
                                                            min_trials_per_class=min_trials_per_class,
                                                            min_n_units=min_n_units)
    print('\n\n\n\n{}'.format(experiment))
    #print(sel_sess)
    print('\n\ntotal # units: {}\n\n'.format(sel_sess['n_units'].sum()))
    time_bins = np.arange(data['data'][0].shape[2])
    n_time_bins_per_trial = data['data'][0].shape[2]
    time_bin_times = time_bins * slide_by_in_ms / 1000 - 0.4

    # SELECT TIMES
    sel_bins = np.logical_and(time_bin_times>t0, time_bin_times<t1)
    time_bins = time_bins[sel_bins]
    time_bin_times = time_bin_times[sel_bins]


    for pi, subpop_size in enumerate(subpopulation_sizes) :

        for boot in range(n_bootstraps):

            # choose random trial indices (different for each neuron)
            random_trial_inds = {}
            neuron_ids_all = []
            for sess_ind, sess_id in zip(sel_sess_ind, sel_sess_id):

                da = data['data'][sess_ind]
                neuron_inds = np.arange(da.shape[1])
                neuron_ids = ['unit_{}_{}'.format(n, sess_ind) for n in neuron_inds]
                neuron_ids_all.append(neuron_ids)
                dt = sel_trials[sel_trials['session_id'] == sess_id]
                ind0 = dt[dt['label'] == 0]['trial_ind'].__array__()
                ind1 = dt[dt['label'] == 1]['trial_ind'].__array__()
                assert set(ind0).intersection(set(ind1)).__len__() == 0

                randind = np.zeros(shape=[2*min_trials_per_class, len(neuron_inds)])
                for neuron_ind in neuron_inds :
                    randind0 = np.random.choice(ind0, size=min_trials_per_class,
                                                replace=False)
                    randind1 = np.random.choice(ind1, size=min_trials_per_class,
                                                replace=False)
                    randind[:, neuron_ind] = np.hstack([randind0, randind1])
                random_trial_inds[sess_id] = randind.astype(int)

            # choose random neurons
            neuron_ids = np.hstack(neuron_ids_all)
            n_neurons = np.hstack([random_trial_inds[k] for k in random_trial_inds.keys()]).shape[1]
            random_neuron_inds = np.random.choice(np.arange(n_neurons), size=subpop_size,
                                                  replace=False)
            random_neuron_ids = neuron_ids[random_neuron_inds]


            kfold = StratifiedKFold(n_splits=n_splits, shuffle=True,
                                    random_state=boot)

            for time_bin, time in zip(time_bins, time_bin_times):
                print('pop. size {}/{} bootstrap {}/{} - time bin {}/{} (t={})'.format(
                        pi + 1, len(subpopulation_sizes),
                        boot, n_bootstraps,
                        time_bin, len(time_bins), time))

                X = []

                for sess_ind, sess_id in zip(sel_sess_ind, sel_sess_id):

                    da = data['data'][sess_ind]
                    neuron_inds = np.arange(da.shape[1])

                    for neuron_ind in neuron_inds:
                        # TODO check this step
                        X.append(da[random_trial_inds[sess_id][:, neuron_ind], neuron_ind, time_bin][:, np.newaxis])

                X = np.hstack(X)

                X = X[:, random_neuron_inds]

                y = np.hstack((np.zeros(min_trials_per_class), np.ones(min_trials_per_class)))

                for fold, (training_ind, testing_ind) in enumerate(kfold.split(X, y)) :
                    # print(training_ind[0:5])
                    X_train = X[training_ind, :]
                    X_test = X[testing_ind, :]
                    y_train = y[training_ind]
                    y_test = y[testing_ind]

                    # print(y_train.shape[0], y_train.sum())
                    ss = StandardScaler()
                    X_train = ss.fit_transform(X_train)
                    X_test = ss.transform(X_test)

                    decoder = SGDClassifier()
                    random_forest = RandomForestClassifier(n_estimators=n_estimators)
                    decoder.fit(X_train, y_train)
                    y_pred = decoder.predict(X_test)
                    score = accuracy_score(y_test, y_pred)

                    r = permutation_importance(decoder, X_test, y_test,
                                               n_repeats=n_perm_repeats)

                    df = pd.DataFrame(columns=['unit_id',
                                 'experiment', 'time', 'bootstrap', 'fold',
                                 'population_size', 'svm_accuracy',
                                 'svm_perm_imp', 'svm_perm_imp_std',
                                 'svm_coef'])

                    df['unit_id'] = random_neuron_ids
                    df['experiment'] = experiment
                    df['time'] = time
                    df['bootstrap'] = boot
                    df['fold'] = fold
                    df['population_size'] = len(random_neuron_ids)
                    df['svm_accuracy'] = score
                    df['svm_perm_imp'] = r.importances_mean
                    df['svm_perm_imp_std'] = r.importances_std
                    df['svm_coef'] = decoder.coef_[0]

                    numeric_cols = ['svm_perm_imp', 'svm_perm_imp_std',
                                    'svm_coef']

                    for col in numeric_cols :
                        df[col] = pd.to_numeric(df[col])

                    dfs.append(df)


df = pd.concat(dfs)

pars = {'settings_name' : settings_name,
        'experiments' : experiments,
        'binsize_in_ms' : binsize_in_ms,
        'slide_by_in_ms' : slide_by_in_ms,
        'subpopulation_sizes' : subpopulation_sizes,
        'min_trials_per_class' : min_trials,
        'min_n_units' : min_n_units,
        'decoder_name' : decoder_name,
        'n_estimators' : n_estimators,
        'n_splits' : n_splits,
        'n_bootstraps' : n_bootstraps,
        'n_perm_repeats' : n_perm_repeats,
        't0' : t0,
        't1' : t1}

out = {'pars' : pars,
       'feature_importances' : df}

print('Saving output to {}'.format(output_full_path))
pickle.dump(out, open(output_full_path, 'wb'))










