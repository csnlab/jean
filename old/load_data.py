import os
from constants import *
from loadmat import *
import pandas as pd
from utils import *

"""
- separate high and low saliency and left and right
- get svm coefficient to find important neurons across conditions
- then look at best unisensory decoder vs  
"""

# ----
settings_name = 'mar2_highsal'
area = 'PPC'
binsize_in_ms = 100
slide_by_in_ms = 25
min_n_units = 1
n_bootstraps = 100
subpopulation_sizes = [150]
n_splits = 3
decoder_name = 'SVM'
n_estimators = 200
trial_side = None
trial_saliency = 'low'

data, trial_data = load_data()
key = 'data{}'.format(area)
session_ids = trial_data['session_id'].unique()
session_inds = np.arange(data[key].shape[0])

experiments = ['visual_presence']

min_trials = {'presence' : 60,
              'hitmiss' : 25}

output_file_name = 'jeandecode_{}.pkl'.format(settings_name)
output_folder = os.path.join(DATA_PATH, 'results', 'pseudodec', settings_name)
output_full_path = os.path.join(output_folder, output_file_name)

if not os.path.isdir(output_folder) :
    os.makedirs(output_folder)

df = pd.DataFrame(columns=['experiment', 'group_size',
                           'time', 't0', 't1', 'bootstrap', 'score'])

for experiment in experiments :

    if experiment[-8 :] == 'presence' :
        min_trials_per_class = min_trials['presence']
    elif experiment[-7 :] == 'hitmiss' :
        min_trials_per_class = min_trials['hitmiss']

    # sel_sess, sel_sess_ind, sel_sess_id, sel_trials = select_sessions(data,
    #                                                                   trial_data,
    #                                                                   experiment,
    #                                                                   min_trials_per_class=min_trials_per_class,
    #                                                                   min_n_units=min_n_units,
    #                                                                   trial_side=trial_side,
    #                                                                   trial_saliency=trial_saliency)
    #
    # print('\n\n\n\n{}'.format(experiment))
    # # print(sel_sess)
    # print('\n\ntotal # units: {}\n\n'.format(sel_sess['n_units'].sum()))
    # time_bins = np.arange(data[key][0].shape[2])
    # n_time_bins_per_trial = data[key][0].shape[2]
    # time_bin_times = time_bins * slide_by_in_ms / 1000 - 0.4

    key = 'data{}'.format(area)

    session_inds = np.arange(data[key].shape[0])
    session_ids = ['sess{:02d}'.format(s) for s in session_inds]

    if experiment == 'multisensory_presence':

        sel_trials = trial_data[np.isin(trial_data['trial_type'], ['M', 'C'])]
        sel_trials['label'] = [0 if t == 'C' else 1 for t in
                               sel_trials['trial_type']]

    elif experiment == 'unisensory_presence' :
        sel_trials = trial_data[
            np.isin(trial_data['trial_type'], ['V', 'T', 'C'])]
        sel_trials['label'] = [0 if t == 'C' else 1 for t in
                               sel_trials['trial_type']]

    elif experiment == 'visual_presence' :
        sel_trials = trial_data[np.isin(trial_data['trial_type'], ['V', 'C'])]
        sel_trials['label'] = [0 if t == 'C' else 1 for t in
                               sel_trials['trial_type']]

    elif experiment == 'tactile_presence' :
        sel_trials = trial_data[np.isin(trial_data['trial_type'], ['T', 'C'])]
        sel_trials['label'] = [0 if t == 'C' else 1 for t in
                               sel_trials['trial_type']]

    elif experiment == 'multisensory_hitmiss' :
        sel_trials = trial_data[np.isin(trial_data['trial_type'], ['M'])]
        sel_trials['label'] = [1 if t == 'correct' else 0 for t in
                               sel_trials['trial_outcome']]

    elif experiment == 'visual_hitmiss' :
        sel_trials = trial_data[np.isin(trial_data['trial_type'], ['V'])]
        sel_trials['label'] = [1 if t == 'correct' else 0 for t in
                               sel_trials['trial_outcome']]

    elif experiment == 'tactile_hitmiss' :
        sel_trials = trial_data[np.isin(trial_data['trial_type'], ['T'])]
        sel_trials['label'] = [1 if t == 'correct' else 0 for t in
                               sel_trials['trial_outcome']]

    elif experiment == 'unisensory_hitmiss' :
        sel_trials = trial_data[np.isin(trial_data['trial_type'], ['V', 'T'])]
        sel_trials['label'] = [1 if t == 'correct' else 0 for t in
                               sel_trials['trial_outcome']]

    else :
        raise ValueError

    if trial_saliency is not None:
        if np.isin(experiment, presence_experiments):
            sel_trials = sel_trials[np.isin(sel_trials['trial_saliency'], ['catch', trial_saliency])]
        elif np.isin(experiment, hitmiss_experiments) :
            sel_trials = sel_trials[sel_trials['trial_saliency'] == trial_saliency]

    if trial_side is not None :
        sel_trials = sel_trials[sel_trials['trial_side'] == trial_side]

    sess_n_trials = pd.DataFrame(columns=['session_id', 'n0', 'n1', 'n_units'])

    for sess_ind, sess_id in zip(session_inds, session_ids) :
        n_neurons = data[key][sess_ind].shape[1]
        valc = sel_trials[sel_trials['session_id'] == sess_id][
            'label'].value_counts()
        try :
            val0 = valc[0]
        except KeyError :
            val0 = 0

        try :
            val1 = valc[1]
        except KeyError :
            val1 = 0

        sess_n_trials.loc[sess_n_trials.shape[0], :] = [sess_id, val0,
                                                        val1, n_neurons]

    sel_sess = sess_n_trials[(sess_n_trials['n0'] >= min_trials_per_class) &
                             (sess_n_trials['n1'] >= min_trials_per_class) &
                             (sess_n_trials['n_units'] >= min_n_units)]

    sel_sess_ind = sel_sess['session_id'].index.__array__()
    sel_sess_id = sel_sess['session_id'].__array__()

    sel_trials = sel_trials[np.isin(sel_trials['session_id'], sel_sess)]
