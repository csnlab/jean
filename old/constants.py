
DATA_PATH = '/Users/pietro/data/jean'


trial_type_dict = {67 : 'C',
                   86 : 'V',
                   84 : 'T',
                   77 : 'M'}

mouse_choice_dict = {82 : 'R',
                     76 : 'L',
                     78 : 'N'}

trial_outcome_dict = {1 : 'correct',
                      2 : 'incorrect',
                      3 : 'miss'}


trial_side_dict = {76 : 'L',
                   82 : 'R',
                   67 : 'C'}

trial_saliency_dict = {0 : 'catch',
                       1 : 'low',
                       2 : 'high',
                       3 : 'mix'}



presence_experiments = ['multisensory_presence',
               'unisensory_presence',
               'visual_presence',
               'tactile_presence']

hitmiss_experiments = ['multisensory_hitmiss',
               'visual_hitmiss',
               'tactile_hitmiss',
               'unisensory_hitmiss']
