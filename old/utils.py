import os
from loadmat import loadmat
from constants import *
import numpy as np
import pandas as pd


def load_data() :
    data_path = os.path.join(DATA_PATH, 'JP_data_PPC_V1.mat')

    data = loadmat(data_path)
    session_inds = np.arange(data['dataPPC'].shape[0])
    session_ids = ['sess{:02d}'.format(s) for s in session_inds]

    tds = []
    for sess_ind, sess_id in zip(session_inds, session_ids) :
        labels = data['labels'][sess_ind]

        trial_inds = np.arange(data['labels'][sess_ind].shape[0])
        trial_ids = ['trial{:02d}'.format(s) for s in trial_inds]
        trial_type = [trial_type_dict[l] if not np.isnan(l) else 'nan' for l in
                      labels[:, 0]]
        mouse_choice = [mouse_choice_dict[l] if not np.isnan(l) else 'nan' for l
                        in labels[:, 1]]
        trial_outcome = [trial_outcome_dict[l] if not np.isnan(l) else 'nan' for
                         l in labels[:, 2]]
        trial_side = [trial_side_dict[l] if not np.isnan(l) else 'nan' for
                      l in labels[:, 3]]
        trial_saliency = [trial_saliency_dict[l] if not np.isnan(l) else 'nan'
                          for
                          l in labels[:, 4]]

        td = pd.DataFrame(columns=['session_ind', 'session_id',
                                   'trial_ind', 'trial_id', 'trial_type',
                                   'mouse_choice', 'trial_outcome'])
        td['trial_ind'] = trial_inds
        td['trial_id'] = trial_ids
        td['trial_type'] = trial_type
        td['mouse_choice'] = mouse_choice
        td['trial_outcome'] = trial_outcome
        td['trial_side'] = trial_side
        td['trial_saliency'] = trial_saliency
        td['session_ind'] = sess_ind
        td['session_id'] = sess_id
        tds.append(td)

    trial_data = pd.concat(tds)

    return data, trial_data


def select_sessions(data, trial_data, experiment, min_trials_per_class, min_n_units=1,
                    trial_saliency=None, trial_side=None, area='PPC'):

    key = 'data{}'.format(area)

    session_inds = np.arange(data[key].shape[0])
    session_ids = ['sess{:02d}'.format(s) for s in session_inds]

    if experiment == 'multisensory_presence' :

        sel_trials = trial_data[np.isin(trial_data['trial_type'], ['M', 'C'])]
        sel_trials['label'] = [0 if t == 'C' else 1 for t in
                               sel_trials['trial_type']]

    elif experiment == 'unisensory_presence' :
        sel_trials = trial_data[
            np.isin(trial_data['trial_type'], ['V', 'T', 'C'])]
        sel_trials['label'] = [0 if t == 'C' else 1 for t in
                               sel_trials['trial_type']]

    elif experiment == 'visual_presence' :
        sel_trials = trial_data[np.isin(trial_data['trial_type'], ['V', 'C'])]
        sel_trials['label'] = [0 if t == 'C' else 1 for t in
                               sel_trials['trial_type']]

    elif experiment == 'tactile_presence' :
        sel_trials = trial_data[np.isin(trial_data['trial_type'], ['T', 'C'])]
        sel_trials['label'] = [0 if t == 'C' else 1 for t in
                               sel_trials['trial_type']]

    elif experiment == 'multisensory_hitmiss' :
        sel_trials = trial_data[np.isin(trial_data['trial_type'], ['M'])]
        sel_trials['label'] = [1 if t == 'correct' else 0 for t in
                               sel_trials['trial_outcome']]

    elif experiment == 'visual_hitmiss' :
        sel_trials = trial_data[np.isin(trial_data['trial_type'], ['V'])]
        sel_trials['label'] = [1 if t == 'correct' else 0 for t in
                               sel_trials['trial_outcome']]

    elif experiment == 'tactile_hitmiss' :
        sel_trials = trial_data[np.isin(trial_data['trial_type'], ['T'])]
        sel_trials['label'] = [1 if t == 'correct' else 0 for t in
                               sel_trials['trial_outcome']]

    elif experiment == 'unisensory_hitmiss' :
        sel_trials = trial_data[np.isin(trial_data['trial_type'], ['V', 'T'])]
        sel_trials['label'] = [1 if t == 'correct' else 0 for t in
                               sel_trials['trial_outcome']]

    else :
        raise ValueError

    if trial_saliency is not None:
        if np.isin(experiment, presence_experiments):
            sel_trials = sel_trials[np.isin(sel_trials['trial_saliency'], ['catch', trial_saliency])]
        elif np.isin(experiment, hitmiss_experiments) :
            sel_trials = sel_trials[sel_trials['trial_saliency'] == trial_saliency]

    if trial_side is not None:
        sel_trials = sel_trials[sel_trials['trial_side'] == trial_side]


    sess_n_trials = pd.DataFrame(columns=['session_id', 'n0', 'n1', 'n_units'])

    for sess_ind, sess_id in zip(session_inds, session_ids) :
        n_neurons = data[key][sess_ind].shape[1]
        valc = sel_trials[sel_trials['session_id'] == sess_id][
            'label'].value_counts()
        try:
            val0 = valc[0]
        except KeyError:
            val0 = 0

        try:
            val1 = valc[1]
        except KeyError:
            val1 = 0

        sess_n_trials.loc[sess_n_trials.shape[0], :] = [sess_id, val0,
                                                        val1, n_neurons]

    sel_sess = sess_n_trials[(sess_n_trials['n0'] >= min_trials_per_class) &
                             (sess_n_trials['n1'] >= min_trials_per_class) &
                             (sess_n_trials['n_units'] >= min_n_units)]

    sel_sess_ind = sel_sess['session_id'].index.__array__()
    sel_sess_id = sel_sess['session_id'].__array__()

    sel_trials = sel_trials[np.isin(sel_trials['session_id'], sel_sess)]

    return sel_sess, sel_sess_ind, sel_sess_id, sel_trials
