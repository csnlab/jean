import os
from constants import *
from loadmat import *
import pandas as pd
from sklearn.model_selection import StratifiedKFold
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
from utils import *
import pickle


"""
- separate high and low saliency and left and right
- get svm coefficient to find important neurons across conditions
- then look at best unisensory decoder vs  
"""


# ----
settings_name = 'mar4'
area = 'V1'
binsize_in_ms = 100
slide_by_in_ms = 25
min_n_units = 1
n_bootstraps = 100
subpopulation_sizes = [100]
n_splits = 3
decoder_name = 'SVM'
n_estimators = 200
trial_side = None
trial_saliency = None#'high'

data, trial_data = load_data()
key = 'data{}'.format(area)
session_ids = trial_data['session_id'].unique()
session_inds = np.arange(data[key].shape[0])

experiments = ['multisensory_presence',
               'unisensory_presence',
               'visual_presence',
               'tactile_presence',
               'multisensory_hitmiss',
               'visual_hitmiss',
               'tactile_hitmiss',
               'unisensory_hitmiss']

if trial_saliency is None:
    min_trials = {'presence' : 60,
                  'hitmiss'  : 30}  # if all saliencies combined

else:
    min_trials = {'presence' : 25,
                  'hitmiss'  : 15}  # if all saliencies combined


output_file_name = 'jeandecode_{}_{}.pkl'.format(settings_name, area)
output_folder = os.path.join(DATA_PATH, 'results', 'pseudodec', settings_name)
output_full_path = os.path.join(output_folder, output_file_name)

if not os.path.isdir(output_folder) :
    os.makedirs(output_folder)


df = pd.DataFrame(columns=['experiment', 'group_size',
                           'time', 't0', 't1', 'bootstrap', 'score'])

for experiment in experiments:

    if np.isin(experiment, presence_experiments):
        min_trials_per_class = min_trials['presence']
    elif np.isin(experiment, hitmiss_experiments):
        min_trials_per_class = min_trials['hitmiss']

    sel_sess, sel_sess_ind, sel_sess_id, sel_trials = select_sessions(data,
                                                            trial_data,
                                                            experiment,
                                                            min_trials_per_class=min_trials_per_class,
                                                            min_n_units=min_n_units,
                                                            trial_side=trial_side,
                                                            trial_saliency=trial_saliency)


    print('\n\n\n\n{}'.format(experiment))
    #print(sel_sess)
    print('\n\ntotal # units: {}\n\n'.format(sel_sess['n_units'].sum()))

    if sel_sess['n_units'].sum() > np.max(subpopulation_sizes):

        time_bins = np.arange(data[key][0].shape[2])
        n_time_bins_per_trial = data[key][0].shape[2]
        time_bin_times = time_bins * slide_by_in_ms / 1000 - 0.4

        for pi, subpop_size in enumerate(subpopulation_sizes) :

            for boot in range(n_bootstraps):

                # choose random trial indices (different for each neuron)
                random_trial_inds = {}
                for sess_ind, sess_id in zip(sel_sess_ind, sel_sess_id):

                    da = data[key][sess_ind]
                    neuron_inds = np.arange(da.shape[1])
                    dt = sel_trials[sel_trials['session_id'] == sess_id]
                    ind0 = dt[dt['label'] == 0]['trial_ind'].__array__()
                    ind1 = dt[dt['label'] == 1]['trial_ind'].__array__()
                    assert set(ind0).intersection(set(ind1)).__len__() == 0

                    randind = np.zeros(shape=[2*min_trials_per_class, len(neuron_inds)])
                    for neuron_ind in neuron_inds :
                        randind0 = np.random.choice(ind0, size=min_trials_per_class,
                                                    replace=False)
                        randind1 = np.random.choice(ind1, size=min_trials_per_class,
                                                    replace=False)
                        randind[:, neuron_ind] = np.hstack([randind0, randind1])
                    random_trial_inds[sess_id] = randind.astype(int)

                # choose random neurons
                n_neurons = np.hstack([random_trial_inds[k] for k in random_trial_inds.keys()]).shape[1]
                random_neuron_inds = np.random.choice(np.arange(n_neurons), size=subpop_size,
                                                      replace=False)

                kfold = StratifiedKFold(n_splits=n_splits, shuffle=True,
                                        random_state=boot)

                for time_bin, time in zip(time_bins, time_bin_times):

                    X = []

                    for sess_ind, sess_id in zip(sel_sess_ind, sel_sess_id):

                        da = data[key][sess_ind]
                        neuron_inds = np.arange(da.shape[1])

                        for neuron_ind in neuron_inds:
                            # TODO check this step
                            X.append(da[random_trial_inds[sess_id][:, neuron_ind], neuron_ind, time_bin][:, np.newaxis])

                    X = np.hstack(X)

                    X = X[:, random_neuron_inds]

                    y = np.hstack((np.zeros(min_trials_per_class), np.ones(min_trials_per_class)))

                    kfold_scores = []
                    y_test_all, y_pred_all = [], []

                    for fold, (training_ind, testing_ind) in enumerate(
                            kfold.split(X, y)) :

                        X_train = X[training_ind, :]
                        X_test = X[testing_ind, :]
                        y_train = y[training_ind]
                        y_test = y[testing_ind]

                        ss = StandardScaler()
                        X_train = ss.fit_transform(X_train)
                        X_test = ss.transform(X_test)

                        if decoder_name == 'random_forest' :
                            decoder = RandomForestClassifier(n_estimators=n_estimators)
                        elif decoder_name == 'SVM' :
                            decoder = SGDClassifier()
                        else :
                            raise NotImplementedError

                        decoder.fit(X_train, y_train)
                        y_pred = decoder.predict(X_test)
                        score = accuracy_score(y_test, y_pred)
                        kfold_scores.append(score)
                        y_test_all.append(y_test)
                        y_pred_all.append(y_pred)

                    y_test_all = np.hstack(y_test_all)
                    y_pred_all = np.hstack(y_pred_all)

                    score = np.mean(kfold_scores)

                    print('pop. size {}/{} bootstrap {}/{} - time bin {}/{} (t={}) - score={:.2f}'.format(
                            pi + 1, len(subpopulation_sizes),
                            boot, n_bootstraps,
                            time_bin, n_time_bins_per_trial, time, score))

                    row = [experiment,
                           subpop_size, time, None, None, boot, score]

                    df.loc[df.shape[0], :] = row



pars = {'settings_name' : settings_name,
        'area' : area,
        'experiments' : experiments,
        'binsize_in_ms' : binsize_in_ms,
        'slide_by_in_ms' : slide_by_in_ms,
        'subpopulation_sizes' : subpopulation_sizes,
        'min_trials_per_class' : min_trials,
        'min_n_units' : min_n_units,
        'decoder_name' : decoder_name,
        'n_estimators' : n_estimators,
        'n_splits' : n_splits,
        'n_bootstraps' : n_bootstraps,
        'trial_saliency' : trial_saliency,
        'trial_side' : trial_side}

out = {'pars' : pars,
       'decoding_scores' : df}

print('Saving output to {}'.format(output_full_path))
pickle.dump(out, open(output_full_path, 'wb'))










