import seaborn as sns

experiment_palette = {'multisensory_presence' : sns.xkcd_rgb['purple'],
                      'unisensory_presence'   : sns.xkcd_rgb['teal'],
                      'visual_presence'       : sns.xkcd_rgb['blue'],
                      'tactile_presence'      : sns.xkcd_rgb['red'],
                      'multisensory_hitmiss' : sns.xkcd_rgb['purple'],
                      'unisensory_hitmiss' : sns.xkcd_rgb['teal'],
                      'visual_hitmiss' : sns.xkcd_rgb['blue'],
                      'tactile_hitmiss' : sns.xkcd_rgb['red']}


experiment_label = {'multisensory_presence' : 'stim. presence (M)',
                      'unisensory_presence'   : 'stim. presence (U)',
                      'visual_presence'       : 'stim. presence (V)',
                      'tactile_presence'      : 'stim. presence (T)',
                      'multisensory_hitmiss' : 'hit/miss (M)',
                      'unisensory_hitmiss' : 'hit/miss (U)',
                      'visual_hitmiss' : 'hit/miss (V)',
                      'tactile_hitmiss' : 'hit/miss (T)'}