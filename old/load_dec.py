import os
from constants import *
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import pickle
import numpy as np
from plotting_style import *


settings_name = 'mar4_highsal'
area = 'V1'

save_plots = True
plot_format = 'png'
dpi = 400
plot_folder = os.path.join(DATA_PATH, 'plots')
if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder, exist_ok=True)

output_file_name = 'jeandecode_{}_{}.pkl'.format(settings_name, area)
output_folder = os.path.join(DATA_PATH, 'results', 'pseudodec', settings_name)
output_full_path = os.path.join(output_folder, output_file_name)
results = pickle.load(open(output_full_path, 'rb'))

df = results['decoding_scores']


df['score'] = pd.to_numeric(df['score'])

df['time'] = df['time']




f, ax = plt.subplots(1, 1, figsize=[3, 3])

sns.lineplot(data=df[np.isin(df['experiment'], presence_experiments)],
             x='time', y='score', ax=ax, hue='experiment',
             palette=experiment_palette, err_kws={'linewidth': 0},  ci='sd')

#ax.axvline(-0.05)
#ax.axvline(0.05)
ax.axhline(0.5, c='grey', ls=':')
ax.axvline(0, c='grey', ls=':')
ax.set_xlabel('Time [s]')
ax.set_ylabel('Decoding accuracy')
# ax.set_ylim(ylims)
ax.legend().remove()
sns.despine()
plt.tight_layout()

if save_plots:
    plot_name = 'dec_presence_overtime_settings_{}_{}.{}'.format(settings_name, area,  plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)





f, ax = plt.subplots(1, 1, figsize=[3, 3])

sns.lineplot(data=df[np.isin(df['experiment'], hitmiss_experiments)],
             x='time', y='score', ax=ax, hue='experiment',
             palette=experiment_palette, err_kws={'linewidth': 0},  ci='sd')

#ax.axvline(-0.05)
#ax.axvline(0.05)
ax.axhline(0.5, c='grey', ls=':')
ax.axvline(0, c='grey', ls=':')
ax.set_xlabel('Time [s]')
ax.set_ylabel('Decoding accuracy')
# ax.set_ylim(ylims)
ax.legend().remove()
sns.despine()
plt.tight_layout()

if save_plots:
    plot_name = 'dec_hitmiss_overtime_settings_{}_{}.{}'.format(settings_name, area, plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)