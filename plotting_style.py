import seaborn as sns

laser_palette = {'on' : sns.xkcd_rgb['electric blue'],
                 'off' : sns.xkcd_rgb['brownish orange']}


PPC_color = sns.xkcd_rgb['teal']


dashes_dict = {'off' : '',
               'on' : [5, 2]}
