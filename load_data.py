import os
from constants import *
from loadmat import *
import pandas as pd

# TODO what to do with valid sessions?

def get_data():

    # load data
    data = loadmat(os.path.join(DATA_PATH, '{}.mat'.format(DATA_VERSION)))

    # make binned_spikes dictionary
    mats = data['scores']
    session_ids = ['S{:02d}'.format(i+1) for i in range(mats.__len__())]
    binned_spikes = {s : m for s, m in zip(session_ids, mats)}

    valid_sessions = ['S{:02d}'.format(i) for i in  data['valid_sessions']]

    # compute time bin centers
    time_bin_edges = data['edges']
    binsize = time_bin_edges[0, 1] - time_bin_edges[0, 0]
    time_bin_centers = [np.round(e[0] + binsize/2, 4) for e in time_bin_edges]

    # make trial frame
    tfs = []
    for j, sess_id in enumerate(session_ids):

        if mats[j].shape[0] != 0:
            labels = data['labels'][j]
            assert len(labels) == mats[j].shape[0]

            response, laser = [], []
            for l in labels:
                if np.isin(l, [1, 4]):
                    response.append('hit')
                elif np.isin(l, [2, 5]):
                    response.append('error')
                else:
                    response.append('miss')

                if np.isin(l, [1, 2, 3]):
                    laser.append('off')
                else:
                    laser.append('on')

            trial_ids = ['{}_T{:02d}'.format(sess_id, t+1) for t in range(len(labels))]
            tf = pd.DataFrame(columns=['session_id', 'trial_id', 'response', 'laser'])
            tf['response'] = response
            tf['laser'] = laser
            tf['session_id'] = sess_id
            tf['trial_id'] = trial_ids
            tfs.append(tf)
        tf = pd.concat(tfs)
    tf.index = tf['trial_id']

    # repackage
    mydata = {}
    mydata['binned_spikes'] = binned_spikes
    mydata['time_bin_centers'] = time_bin_centers
    mydata['tf'] = tf
    mydata['session_ids'] = session_ids
    mydata['valid_sessions'] = valid_sessions

    return mydata



