import os
from load_data import get_data
import pandas as pd
import numpy as np
import pickle
from sklearn.model_selection import StratifiedKFold
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import SGDClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import balanced_accuracy_score
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import LabelEncoder
from constants import *

settings_name = 'test_rf'

min_units = 10
min_trials_per_class = 8
decoder_name = 'random_forest'
n_estimators = 500
n_splits = 5
n_repeats = 5

data = get_data()
binned_spikes = data['binned_spikes']
tf = data['tf']
time_bin_centers = data['time_bin_centers']
session_ids = data['session_ids']
valid_sessions = data['valid_sessions']

experiments = ['hit_vs_error',
               'hit_vs_miss',
               'error_vs_miss']


df = pd.DataFrame(columns=['session_id', 'laser', 'experiment',
                           'decoder', 'time', 'repeat', 'score', 'n_neurons'])

for experiment in experiments:
    if experiment == 'hit_vs_error':
        responses = ['hit', 'error']
    elif experiment == 'hit_vs_miss':
        responses = ['hit', 'miss']
    elif experiment == 'error_vs_miss':
        responses = ['error', 'miss']

    for laser in ['on', 'off']:
        for sess_id in valid_sessions:

            # select trials
            tf_sel = tf[tf['session_id'] == sess_id]
            mask = np.logical_and(tf_sel['laser'] == laser,
                                  np.isin(tf_sel['response'], responses))

            # make target
            target = tf_sel[mask]['response']
            vc = target.value_counts()

            ll = LabelEncoder()
            y = ll.fit_transform(target)

            if vc.min() > min_trials_per_class and binned_spikes[sess_id].shape[1] > min_units:


                for i, time in enumerate(time_bin_centers):

                    X = binned_spikes[sess_id][:, :, i]
                    X = X[mask, :]

                    for repeat in range(n_repeats):

                        # print('- Repeat {}/{}'.format(repeat, n_repeats))

                        kfold = StratifiedKFold(n_splits=n_splits, shuffle=True,
                                                random_state=repeat)
                        kfold_scores = []
                        y_test_all, y_pred_all = [], []

                        for fold, (training_ind, testing_ind) in enumerate(
                                kfold.split(X, y)) :

                            if decoder_name == 'random_forest' :
                                decoder = RandomForestClassifier(
                                    n_estimators=n_estimators)
                            elif decoder_name == 'SGD' :
                                decoder = SGDClassifier()
                            else :
                                raise NotImplementedError

                            X_train = X[training_ind, :]
                            X_test = X[testing_ind, :]
                            y_train = y[training_ind]
                            y_test = y[testing_ind]

                            ss = StandardScaler()
                            X_train = ss.fit_transform(X_train)
                            X_test = ss.transform(X_test)

                            decoder.fit(X_train, y_train)
                            y_pred = decoder.predict(X_test)
                            # TODO balanced?
                            scoring_function = balanced_accuracy_score
                            score = scoring_function(y_test, y_pred)
                            kfold_scores.append(score)
                            y_test_all.append(y_test)
                            y_pred_all.append(y_pred)

                        y_test_all = np.hstack(y_test_all)
                        y_pred_all = np.hstack(y_pred_all)

                        mean_score = np.mean(kfold_scores)

                        row = [sess_id, laser, experiment,
                               decoder_name, time, repeat, mean_score,
                               X.shape[1]]

                        df.loc[df.shape[0], :] = row



numeric_cols = ['score', 'time']

for col in numeric_cols :
    df[col] = pd.to_numeric(df[col])


pars = {'settings_name' : settings_name,
        'data_version' : DATA_VERSION,
        'experiments' : experiments,
        'min_trials_per_class' : min_trials_per_class,
        'min_units' : min_units,
        'decoder_name' : decoder_name,
        'n_splits' : n_splits,
        'spike_bin_centers' : time_bin_centers,
        'n_repeats' : n_repeats}


out = {'pars' : pars,
       'decoding_scores' : df}

output_file_name = 'decode_setting_{}.pkl'.format(settings_name)
output_folder = os.path.join(DATA_PATH, 'results', 'decode', settings_name)
output_full_path = os.path.join(output_folder, output_file_name)

if not os.path.isdir(output_folder) :
    os.makedirs(output_folder)

print('Saving output to {}'.format(output_full_path))
pickle.dump(out, open(output_full_path, 'wb'))

